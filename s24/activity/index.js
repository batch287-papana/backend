


const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = ['258 Washington', 'Ave Nw', 'California 90011'];

console.log(`I live at ${address[0]} ${address[1]}, ${address[2]}.`);

const animal = {
	name: 'Lolong',
	species: 'saltwater crocodile',
	weight: '1075',
	length: '20ft 3in'
};

console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} kgs with a measurement of ${animal.length}.`);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach(number => console.log(number));


const sum = numbers.reduce((accumulator, currentvalue) => accumulator+currentvalue, 0);

console.log(sum);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}

let myDog = new Dog();

myDog.name = 'Frankie';
myDog.age = 5;
myDog.breed = 'Miniature Dachshund';

console.log(myDog);

