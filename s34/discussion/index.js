// Use the reqirw directive to load the express module/package
// It allows us to access to methods and functions that will allow us to easily create a server
const express = require("express");

// creates an application using express
// In layman's term, app ia our server
const app = express();

// for our application server to run, we need a port to listen to
const port = 3001;


//Use middleware to allow express to read json
app.use(express.json())

// Use middleware to allow express to be able to read more data types froma response
app.use(express.urlencoded({extended:false}));

// [SECTION] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) =>{
	response.send('Hellow from the /hello endpoint!');
});

app.post("/display-name", (req, res) => {
	res.send(`Hellow there ${req.body.firstName} ${req.body.lastName}!`);
});

// Sign-up in our users.

let users = [];

app.post("/signup", (request, response)=>{
	console.log(request.body);
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
		console.log(users)
	}else{
		response.send("Please input both username and password");
	}
	

});

app.put("/change-password", (req, res)=>{
	let message;
	for(let i=0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`;
			console.log("Newly updated mock database");
			console.log(users);
			break;
		}else{
			message = "User does not exist"
		}
	}
	res.send(message);
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
