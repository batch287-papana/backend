const express = require("express");

const app = express();

const port = 3002;

app.use(express.json())

app.use(express.urlencoded({extended:true}));


app.get("/home", (request, response) =>{
	response.send('Welcome to the homepage');
});

let users = [
	{
		"username": "sreya",
		"password": "sreya"
	},
	{
		"username": "user1",
		"password": "password1"
	},
	{
		"username": "user2",
		"password": "password2"
	},
	{
		"username": "user3",
		"password": "password3"
	}
];

app.get("/users", (request, response) => {
	response.send(users);
});

app.delete("/delete-item", (request, response) =>{
	let message;
	for(let i=0; i<users.length; i++){
		if(request.body.username == users[i].username){
			users.pop(request.body);
			message = `User ${request.body.username} has been deleted.`;
			console.log("Newly updated mock database");
			console.log(users);
			break;
		}else{
			message = "User does not exist"
		}
	}
	response.send(message);
});

app.listen(port, () => console.log(`Server is running at port ${port}`));