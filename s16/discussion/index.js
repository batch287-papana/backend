console.log("Hello");

// [SECTION] Arithmetic Operators
	
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("result of addition operator: " + sum);

	let difference = x - y;
	console.log("result of subtrsction operator: " + difference);

	let product = x*y ;
	console.log("result of multiplication operator: " + product);

	let quotient = x/y ;
	console.log("result of division operator: " + quotient);

	let remainder = y % x;
	console.log("result of modulo operator: " + remainder);

// [SECTION] Assignment Operator(=)

	// Basic Assignment Operator(=)
	// This assignment operator assigns the value of the right operand to a variable and assigns the result to the variable
	let assignmnetNumber = 8;

	// Addition Assignment Operator(+=)
	// The addition assignment operator adds thye value of the right operand to a variable and assigns the result to the variable

	assignmentNumber = assignmnetNumber + 2;
	console.log("Result of addition assignment operator: "+ assignmentNumber);

	// shorthand for assignmentNumber = assignmnetNumber + 2;
	assignmentNumber += 2;
	console.log("result of addition assignmnet operator: " + assignmentNumber);

	// subtractio/multiplication/division assignmnet operator(-=, *= /=)
	assignmentNumber -= 2;
	console.log("result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("result of division assignment operator: " + assignmentNumber);

// Multiple Operators and parentheses

	// PEMDAS Rule (Parenthesis, exponents multiplication, division, addition and subtraction)

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("result of mdas operation: " + mdas);

// [SECTION] Increment and Decrement
	// Operators that adds or subtract values by 1 and reassigns the value of the variable where the increment/ decrement was applied to

	let z = 1;
	let increment = ++z;
    console.log("result of pre-increment: " + increment);
	console.log("result of pre-increment: " + z);

	increment = z++;
	// The value of "z" is at 2 before it was incremented
    console.log("result of post-increment: " + increment);
    // The value of "z" was increased again reassigning the value to 3
	console.log("result of post-increment: " + z);

	let decrement = --z;
	console.log("result of pre-decrement: " + decrement);
	console.log("result of pre-decrement: " + z);

	decrement = z--;
	console.log("result of post-decrement: " + decrement);
	console.log("result of post-decrement: " + z);


// [SECTION] Type Coercion
	// Is the automatic or implicit conversation of values from one data type to another

	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);

	let numF = false+1;
	console.log(numF); 

// [SECTION] Comparision operator

	let juan = 'juan';
	
	// equality operator(==)
	/*
		checks whether the operands are equal/ have the same cntent
		attempts to convert and compare
		returns a boolean value
	*/

	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	console.log('juan' == 'juan');
	console.log('juan' == juan);

	// inequality operator (!=)
	/*
		-checks whether the operands are not equal/have differenr content
		-attempts to convert and compare operands of diff data types
	*/

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);

	// [Strict Equality operator]
	/*
		checks wheteher the operands are equal/ have the same content
		also compares the data types of 2 values
		strict equality operators are better to use in most cases to ensure the data types are provided are correct
	*/

	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);

	// Strict Inequality Operator
	/*
		checks whether the operands are not equal/have the same content
		also compares the data types of 2 values
	*/

	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

// [SECTION] Relational operators
	// Some comparision operators check whether one value is grater or less than to the other value

	let a = 50;
	let b = 65;

	// GT (>)
	let isGreaterthan = a > b;
	console.log(isGreaterthan);
	// LT (<)
	let isLessthan = a < b;
	console.log(isLessthan);

	// GTorEqual(>=), LTorEqual(<=)

// Logical Operators
	let isLegalage = true;
	let isRegistered = false;

	// Logical and operator(&& - Double Ampersand)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalage && isRegistered;
	console.log("Result of logical and operator: "+allRequirementsMet);

	// Logical or operator(|| - Double Pipe)
	let someReqirementsMet = isLegalage || isRegistered;
	console.log("result of or operator: " + someReqirementsMet);

	// Logical not operator
	let someRequirementsNotMet = !isRegistered;
	console.log("result of logical not operator: " + someRequirementsNotMet);



