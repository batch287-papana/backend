/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userDetails(){
		alert("Hello!! Please enter your details")
		let fullName = prompt("What is your full name? ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your current location: ");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old");
		console.log("You live in " + location);

	}

	userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favBands(){
		let band1 = "The Beatles";
		let band2 = "One Direction";
		let band3 = "Pink Floyd";
		let band4 = "The Beach Boys";
		let band5 = "The Rolling Stone";

		console.log("Your top 5 favourite bands/musical artists: ");
		console.log("1. "+band1);
		console.log("2. "+band2);
		console.log("3. "+band3);
		console.log("4. "+band4);
		console.log("5. "+band5);
	}

	favBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function movieRatings(){
		let movie1 = "Avengers:Infinity War";
		let movie2 = "Harry Potter and the Priznor of Azkaban";
		let movie3 = "John Wick: Chapter 3";
		let movie4 = "Legally Blonde";
		let movie5 = "Gaurdians of the galaxy: volume 3";
		let rating1 = "85%";
		let rating2 = "90%";
		let rating3 = "89%";
		let rating4 = "71%";
		let rating5 = "82%";

		console.log("1. " + movie1);
		console.log("Rotten Tomatoes rating :" + rating1);
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes rating :" + rating2);
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes rating :" + rating3);
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes rating :" + rating4);
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes rating :" + rating5);
	}

	movieRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends  = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


//console.log(friend1);
//console.log(friend2);
